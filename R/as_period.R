asUnit <- function(x) {
    unit <- tolower(x)
    unit <- ifelse(
        unit %in% c('month', 'months'), yes = 'month',
                   no = ifelse(
                       unit %in% c('day', 'days'), yes = 'day',
                       no = ifelse(
                           unit %in% c('week', 'weeks'), yes = 'week',
                           no = ifelse(
                               unit %in% c('year', 'years'), yes = 'year',
                               no = ifelse(
                                   unit %in% c('quarter', 'quarters'), yes = 'quarter',
                                   no = NA
                               )
                           )
                       )
                   )
    )
    return(unit)
}



asPeriod <- function(number, unit) {
    unit <- asUnit(unit)

    result <- lubridate::as.period(number*(unit=='month'), 'month') +
        lubridate::days(number*(unit=='day')) +
        lubridate::weeks(number*(unit=='week')) +
        lubridate::years(number*(unit=='year')) +
        lubridate::as.period(number*(unit=='quarter'), 'month')*3 # a quarter is 3 months


    return(result)
}

# Example
# asPeriod(c(1:10),c('month','week','year','day','day','month','week','year','month','month'))
