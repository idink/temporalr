asDate <- function(year, month, day, out.of.range.day = c('last day of the month', 'first day of next month')) {
    year <- as.integer(year)
    month <- as.integer(month)
    day <- as.integer(day)

    out.of.range.day <- match.arg(out.of.range.day)

    num.days.in.month <- getNumOfDaysInMonth(year = year, month = month)

    if(out.of.range.day == 'first day of next month') {
        result <- lonelyr::safeIfElse(
            day > num.days.in.month,
            yes = addPeriod(
                date = as.Date(ISOdate(year = year, month = month, day = 1)),
                number = 1, unit = 'month'
            ),
            no = as.Date(ISOdate(year = year, month = month, day = day))
        )

    } else {
        result <- as.Date(ISOdate(year = year, month = month, day = pmin(day, num.days.in.month)))
    }
    return(result)
}


# Example
# x <- asDate(year = 2016, month = 1, day = 32, out.of.range.day = 'first')
# y <- asDate(year = 2016, month = 1, day = 32, out.of.range.day = 'last')
