convertMonthAbbToNum <- function(x) {
    return(match(x = tolower(x), table = tolower(month.abb)))
}
